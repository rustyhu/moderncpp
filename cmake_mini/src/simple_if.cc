#include "simple_if.h"

#include <iostream>

void simple_func(std::string strPass) {
  std::cout << "Inside simple: " << strPass << "\n";
}
